document.getElementById("game").addEventListener("click", addBall);
let count = 0;

const winRed = document.getElementById("winRed").innerText;
const winBlack = document.getElementById("winBlack").innerText;
let placarRed = 0;
let placarBlack = 0;

let arrayMap = [[], [], [], [], [], [], [], []];

//função PRINCIPAL 
function addBall(evt) {
  typeWin(0, 0, true);
  
  let clickColumn = evt.target;

  if (clickColumn.className === "col") {
    if (count % 2 == 0) {
      if (clickColumn.childElementCount != 6) {
        let ball = document.createElement("div");
        ball.classList.add("redBall");
        clickColumn.appendChild(ball);
        count++;
        for (let j = 0; j <= 6; j++) {
          if (clickColumn.id == "col" + j.toString()) {
            arrayMap[j].push(1);
            //condição VERTICAL
            checkVerticalWinner(arrayMap[j]);
          }
        }
      }
    } else if (count % 2) {
      if (clickColumn.childElementCount != 6) {
        let ball = document.createElement("div");
        ball.classList.add("blackBall");
        clickColumn.appendChild(ball);
        count++;
        for (let j = 0; j <= 6; j++) {
          if (clickColumn.id == "col" + j.toString()) {
            arrayMap[j].push(2);
            // condição VERTICAL
            checkVerticalWinner(arrayMap[j]);
          }
        }
      }
      console.log(arrayMap);
    }
  }

  //condição de EMPATE
  let conta = 0;
  conta +=
    document.getElementsByClassName("blackBall").length +
    document.getElementsByClassName("redBall").length;
  if (conta == 42) {
    mensagemEmpate();
  }
  horizontalWinner(arrayMap);
  diagonalWinner_left_to_Rigth(arrayMap);
  diagonalWinner_right_to_left(arrayMap);
}

document.getElementById("reset").addEventListener("click", clearGame);
function clearGame() {
  document.getElementById("modal").style.visibility = "visible";
  document.getElementById("yes").addEventListener("click", () => {
    document.getElementById("winRed").innerText = 0;
    document.getElementById("winBlack").innerText = 0;
    resetGame();
    document.getElementById("modal").style.visibility = "hidden";
  });

  document.getElementById("no").addEventListener("click", () => {
    document.getElementById("modal").style.visibility = "hidden";
  });
}

function resetGame() {
  let column = document.getElementById("game").children;
  for (let i = 0; i <= 6; i++) {
    column[i].innerHTML = "";
    count = 0;
    arrayMap = [[], [], [], [], [], [], [], []];
  }
}

//verificar WINNER na VERTICAL
function checkVerticalWinner(arr) {
  for (let column = 0; column < arr.length; column++) {
    if (
      arr[column] == arr[column + 1] &&
      arr[column + 1] == arr[column + 2] &&
      arr[column + 2] == arr[column + 3]
    ) {
      let win = arr[column];
      typeWin(win, "vertical");
      if (win === 1) {
        document.getElementById("winRed").innerText++;
        placarRed++;
      } else if (win === 2) {
        document.getElementById("winBlack").innerText++;
        placarBlack++;
      }
      if (placarRed === 4 || placarBlack === 4) {
        playerWin(win);
      }
      resetGame();
    }
  }
}

//verificar WINNER horizontal
function horizontalWinner(arr) {
  for (let columns = 0; columns < arr.length; columns++) {
    for (let cell = 0; cell < arr[columns].length; cell++) {
      if (
        arr[columns][cell] == arr[columns + 1][cell] &&
        arr[columns + 1][cell] == arr[columns + 2][cell] &&
        arr[columns + 2][cell] == arr[columns + 3][cell]
      ) {
        let win = arr[columns][cell];
        typeWin(win, "horizontal");
        if (win === 1) {
          document.getElementById("winRed").innerText++;
          placarRed++;
        } else if (win === 2) {
          document.getElementById("winBlack").innerText++;
          placarBlack++;
        }
        if (placarRed === 4 || placarBlack === 4) {
          playerWin(win);
        }
        resetGame();
      }
    }
  }
}

//verificar WINNER diagonal Esquerada para direita
function diagonalWinner_left_to_Rigth(arr) {
  for (let column = 0; column < arr.length; column++) {
    for (let cell = 0; cell < arr[column].length; cell++) {
      if (
        arr[column][cell] == arr[column + 1][cell + 1] &&
        arr[column + 1][cell + 1] == arr[column + 2][cell + 2] &&
        arr[column + 2][cell + 2] == arr[column + 3][cell + 3]
      ) {
        let win = arr[column][cell];
        typeWin(win, "diagonal");
        if (win === 1) {
          document.getElementById("winRed").innerText++;
          placarRed++;
        } else if (win === 2) {
          document.getElementById("winBlack").innerText++;
          placarBlack++;
        }
        if (placarRed === 4 || placarBlack === 4) {
          playerWin(win);
        }
        resetGame();
      }
    }
  }
}

function diagonalWinner_right_to_left(arr) {
  for (let column = 7; column >= 0; column--) {
    for (let cell = 0; cell < arr[column].length; cell++) {
      if (
        arr[column][cell] === arr[column - 1][cell + 1] &&
        arr[column - 1][cell + 1] === arr[column - 2][cell + 2] &&
        arr[column - 2][cell + 2] === arr[column - 3][cell + 3]
      ) {
        let win = arr[column][cell];
        typeWin(win, "diagonal");
        if (win === 1) {
          document.getElementById("winRed").innerText++;
          placarRed++;
        } else if (win === 2) {
          document.getElementById("winBlack").innerText++;
          placarBlack++;
        }
        if (placarRed === 4 || placarBlack === 4) {
          playerWin(win, "diagonal");
        }
        resetGame();
      }
    }
  }
}

function playerWin(winner) {
  let modal = document.createElement("div");
  modal.classList.add("winGame");
  let paragrafo = document.createElement("p");
  let button = document.createElement("button");
  button.classList.add("restart");
  button.innerText = "Restart Game";
  button.addEventListener("click", () => {
    window.location.reload();
  });
  let gameContainer = document.querySelector("#game-container");
  gameContainer.innerText = "";
  paragrafo.innerText = `Player ${winner} venceu o Game`;
  modal.appendChild(paragrafo);
  modal.appendChild(button);
  gameContainer.appendChild(modal);
}

function typeWin(winner = 0, position = 0, a = false) {
  if (winner === 1) {
    let typeWinRed = document.querySelector("#typeWinRed");
    typeWinRed.innerText = `Show, Player ${winner} ${position} !`;
  }
  if (winner === 2) {
    let typeWinBlack = document.querySelector("#typeWinBlack");
    typeWinBlack.innerText = `Show, Player ${winner} ${position} !`;
  }
  if (a) {
    let typeWinRed = document.querySelector("#typeWinRed");
    typeWinRed.innerText = " ";
    let typeWinBlack = document.querySelector("#typeWinBlack");
    typeWinBlack.innerText = " ";
  }
}

//dark mode
function darkMode() {
  var element = document.body;
  element.classList.toggle("dark-mode");
}

function mensagemEmpate() {
  document.getElementById('typeWinRed').innerText = 'Aí não, empatou!'
  document.getElementById('typeWinBlack').innerText = 'Aí não, empatou!'
}